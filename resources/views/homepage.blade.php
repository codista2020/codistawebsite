@extends('layouts.master')
@section('content')
<section class="page_slider main_slider">
    <div class="flexslider" data-nav="true" data-dots="false">
        <ul class="slides">
            <li class="ds text-center slide1">
                <span class="flexslider-overlay"></span>
                <span class="embed-responsive embed-responsive-16by9">
                    <iframe src="https://www.youtube.com/embed/UBufeh1yv2c?feature=oembed&;showinfo=0&;autoplay=1&;controls=0&;mute=1&;loop=1&;playlist=UBufeh1yv2c" allowfullscreen=""></iframe>
                </span>
                <div class="container">
                    <div class="row">
                        <div class="col-12 itro_slider">
                            <div class="intro_layers_wrapper">
                                <div class="intro_layers">
                                    <div class="intro_layer" data-animation="fadeIn">
                                        <p class="text-uppercase intro_after_featured_word"> {{trans('front.indexWelcome')}}</p>
                                    </div>

                                    <div class="intro_layer" data-animation="slideRight">
                                        <h2 class="text-uppercase intro_featured_word">
                                             {{trans('front.indexSlider1')}}
                                        </h2>
                                    </div>
                                    <div class="intro_layer" data-animation="fadeIn">
                                        <h3 class="intro_before_featured_word">
                                            <span class="color-main2">{{trans('front.indexSlider2')}}</span>,
                                            <span class="color-main3">{{trans('front.indexSlider3')}}</span> &
                                            <span class="color-main4">{{trans('front.indexSlider4')}}</span>
                                        </h3>
                                    </div>
                                    <div class="intro_layer page-bottom" data-animation="expandUp">
                                        <a class="btn btn-maincolor" href="/services">{{trans('front.indexSliderBtn')}}</a>
                                    </div>
                                </div>
                                <!-- eof .intro_layers -->
                            </div>
                            <!-- eof .intro_layers_wrapper -->
                        </div>
                        <!-- eof .col-* -->
                    </div>
                    <!-- eof .row -->
                </div>
                <!-- eof .container -->
            </li>
            <li class="ds text-center slide2">
                <span class="flexslider-overlay"></span>
                <img src="assets/images/slide_02.jpg" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-12 itro_slider">
                            <div class="intro_layers_wrapper">
                                <div class="intro_layers">
                                    <div class="intro_layer" data-animation="fadeIn">
                                        <h3 class="text-lowercase intro_before_featured_word">
                                            {{trans('front.indexSlider5')}}
                                        </h3>
                                    </div>
                                    <div class="intro_layer" data-animation="fadeIn">
                                        <h2 class="text-uppercase intro_featured_word">
                                            {{trans('front.indexSlider6')}}
                                        </h2>
                                    </div>
                                    <div class="intro_layer" data-animation="pullDown">
                                        <p class="text-uppercase intro_after_featured_word">{{trans('front.indexSlider7')}}</p>
                                    </div>
                                    <div class="intro_layer page-bottom" data-animation="expandUp">
                                        <a class="btn btn-maincolor" href="/contact">{{trans('front.indexSliderBtn2')}}</a>
                                    </div>
                                </div>
                                <!-- eof .intro_layers -->
                            </div>
                            <!-- eof .intro_layers_wrapper -->
                        </div>
                        <!-- eof .col-* -->
                    </div>
                    <!-- eof .row -->
                </div>
                <!-- eof .container -->
            </li>
            <li class="ds text-center slide3">
                <img src="assets/images/slide_03.jpg" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-12 itro_slider">
                            <div class="intro_layers_wrapper">
                                <div class="intro_layers">
                                    <div class="intro_layer" data-animation="fadeInRight">
                                        <h2 class="text-uppercase intro_featured_word light">
                                            {{trans('front.indexSlider8')}}
                                        </h2>
                                    </div>
                                    <div class="intro_layer" data-animation="fadeIn">
                                        <h2 class="text-uppercase intro_featured_word bold">
                                            {{trans('front.indexSlider9')}}
                                        </h2>
                                    </div>
                                    <div class="intro_layer" data-animation="fadeIn">
                                        <h2 class="text-uppercase intro_featured_word">
                                            {{trans('front.indexSlider10')}}
                                        </h2>
                                    </div>

                                    <div class="intro_layer page-bottom" data-animation="expandUp">
                                        <a class="btn btn-maincolor" href="/contact">{{trans('front.indexSliderBtn2')}}</a>
                                        <a class="btn btn-outline-maincolor" href="/">{{trans('front.indexSliderBtn3')}}</a>
                                    </div>
                                </div>
                                <!-- eof .intro_layers -->
                            </div>
                            <!-- eof .intro_layers_wrapper -->
                        </div>
                        <!-- eof .col-* -->
                    </div>
                    <!-- eof .row -->
                </div>
                <!-- eof .container -->
            </li>

        </ul>
    </div>
    <!-- eof flexslider -->
    <div class="flexslider-bottom d-none d-xl-block">
        <a href="#about" class="mouse-button animated floating"></a>
    </div>
</section>
<div class="divider-10 d-block d-sm-none"></div>
<section class="s-pt-30 s-pt-lg-50 s-pt-xl-25 ls about-home" id="about">
    <div class="divider-5 d-none d-xl-block"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                <div class="main-content text-center">
                    <div class="img-wrap text-center">
                        <img src="assets/images/vertical_line.png" alt="">
                        <div class="divider-35"></div>
                    </div>
                    <h5>
                        {{trans('front.indexWho1')}}
                    </h5>
                    <p>
                        {{trans('front.indexWho2')}}
                        <strong>{{trans('front.indexWho3')}}</strong> 
                        {{trans('front.indexWho4')}}
                    </p>
                    <div class="divider-30"></div>
                    <div class="img-wrap text-center">
                        <img src="assets/images/vertical_line.png" alt="">
                    </div>
                    <div>
                        <div class="divider-40"></div>
                        <button type="button" class="btn btn-outline-maincolor">{{trans('front.indexSliderBtn')}}</button>
                        <div class="divider-40"></div>
                    </div>
                    <div class="img-wrap text-center">
                        <img src="assets/images/vertical_line.png" alt="">
                    </div>
                    <div class="divider-10 d-none d-xl-block"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="divider-10 d-block d-sm-none"></div>
</section>

<section class="s-pt-30 s-pb-3 service-item2 ls animate" id="services" data-animation="fadeInUp">
    <div class="container">
        <div class="row c-mb-50 c-mb-md-60">
            <div class="d-none d-lg-block divider-20"></div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="vertical-item text-center">
                    <div class="item-media">
                        <img src="assets/images/service_icon_1-1.png" alt="">
                    </div>
                    <div class="item-content">
                        <h6>
                            <a href="/services">{{trans('front.indexSlider6')}}</a>
                        </h6>

                        <p>
                            {{trans('front.indexServicep3')}}
                        </p>

                    </div>
                </div>
            </div>
            <!-- .col-* -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="vertical-item text-center">
                    <div class="item-media">
                        <img src="assets/images/service_icon_2-1.png" alt="">
                    </div>
                    <div class="item-content">
                        <h6>
                            <a href="/services">{{trans('front.indexServicetitle2')}}</a>
                        </h6>

                        <p>
                            {{trans('front.indexServicep2')}}
                        </p>

                    </div>
                </div>
            </div>
            <!-- .col-* -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="vertical-item text-center">
                    <div class="item-media">
                        <img src="assets/images/service_icon_3-1.png" alt="">
                    </div>
                    <div class="item-content">
                        <h6>
                            <a href="/services">{{trans('front.indexServicetitle1')}}</a>
                        </h6>

                        <p>
                            {{trans('front.indexServicep1')}}
                        </p>
                    </div>
                </div>
            </div>
            <!-- .col-* -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="vertical-item text-center">
                    <div class="item-media">
                        <img src="assets/images/service_icon_4-1.png" alt="">
                    </div>
                    <div class="item-content">
                        <h6>
                            <a href="/services">{{trans('front.indexServicetitle6')}}</a>
                        </h6>

                        <p>
                            {{trans('front.indexServicep6')}}
                        </p>
                    </div>
                </div>
            </div>
            <!-- .col-* -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="vertical-item text-center">
                    <div class="item-media">
                        <img src="assets/images/service_icon_5-1.png" alt="">
                    </div>
                    <div class="item-content">
                        <h6>
                            <a href="/services">{{trans('front.indexServicetitle5')}}</a>
                        </h6>

                        <p>
                            {{trans('front.indexServicep5')}}
                        </p>

                    </div>
                </div>
            </div>
            <!-- .col-* -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="vertical-item text-center">
                    <div class="item-media">
                        <img src="assets/images/service_icon_6-1.png" alt="">
                    </div>
                    <div class="item-content">
                        <h6>
                            <a href="/services">{{trans('front.indexServicetitle4')}}</a>
                        </h6>

                        <p>
                            {{trans('front.indexServicep4')}}
                        </p>

                    </div>
                </div>
            </div>
            <!-- .col-* -->
        </div>
        <div class="pink-line text-center">
            <img src="assets/images/pink_line_big.png" alt="">
        </div>
    </div>
</section>


<section class="s-pt-100 s-pt-lg-130 ds process-part skew_right s-parallax top_white_line_big overflow-visible" id="steps">
    <div class="container">
        <div class="divider-65"></div>
        <div class="row align-items-center c-mb-20 c-mb-lg-60" data-aos="zoom-in">
            <div class="col-12 col-lg-4">
                <div class="step-left-part text-right">
                    <h2 class="step-title">
                        <span class="color-main">01</span>{{trans('front.indexPlane1')}}</h2>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="step-center-part text-center">
                    <img src="assets/images/step_img_1.jpg" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="step-right-part">
                    <p class="step-text">
                        {{trans('front.indexPlanep1')}}</p>
                </div>
            </div>
        </div>

        <div class="row align-items-center right c-mb-20 c-mb-lg-60" data-aos="flip-right" data-aos-duration="5000">
            <div class="col-12 col-lg-4  order-lg-3">
                <div class="step-left-part">
                    <h2 class="step-title color1">
                        <span class="color-main2">02</span>{{trans('front.indexPlane2')}}</h2>
                </div>
            </div>
            <div class="col-12 col-lg-4 order-lg-2">
                <div class="step-center-part text-center">
                    <img src="assets/images/step_img_2.jpg" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-4 order-lg-1 text-right">
                <div class="step-right-part ">
                    <p class="step-text">
                        {{trans('front.indexPlanep2')}}</p>
                </div>
            </div>
        </div>

        <div class="row align-items-center c-mb-20 c-mb-lg-60" data-aos="flip-left" data-aos-duration="6000">
            <div class="col-12 col-lg-4">
                <div class="step-left-part text-right part3">
                    <h2 class="step-title">
                        <span class="color-main3">03</span>{{trans('front.indexPlane3')}}</h2>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="step-center-part text-center">
                    <img src="assets/images/step_img_3.jpg" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="step-right-part">
                    <p class="step-text">{{trans('front.indexPlanep3')}}</p>
                </div>
            </div>
        </div>

        <div class="row align-items-center right c-mb-20 c-mb-lg-60" data-aos="flip-right" data-aos-duration="7000">
            <div class="col-12 col-lg-4  order-lg-3">
                <div class="step-left-part part4">
                    <h2 class="step-title color1">
                        <span class="color-main4">04</span>{{trans('front.indexPlane4')}}</h2>
                </div>
            </div>
            <div class="col-12 col-lg-4 order-lg-2">
                <div class="step-center-part text-center last">
                    <img src="assets/images/step_img_2.jpg" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-4 order-lg-1 text-right">
                <div class="step-right-part ">
                    <p class="step-text">{{trans('front.indexPlanep4')}}</p>
                </div>
            </div>
        </div>
        <div class="divider-10 d-block d-sm-none"></div>
        <div class="img-wrap text-center">
            <img src="assets/images/vertical_line2.png" alt="">
        </div>
        <div class="divider-30 d-none d-xl-block"></div>
    </div>
</section>

<section class="s-pt-75 s-pt-xl-50 gallery-carousel main-gallery container-px-0" id="gallery">
    <div class="container-fluid">
        <div class="img-wrap text-center">
            <img src="assets/images/vertical_line.png" alt="">
            <div class="divider-40 d-block d-sm-none"></div>
        </div>
    </div>
</section>



<section class="s-pt-130 s-pb-15 s-pb-md-50 s-pt-xl-100 s-pb-lg-30 overflow-visible s-parallax testimonials-sliders main-testimonials ds" id="testimonials">
    <div class="corner ls ms"></div>
    <div class=" white-button text-center">
        <a class="btn white-btn" href="#testimonials">
            {{trans('front.indexCustomerBtn')}}
        </a>
    </div>
    <div class="container">
        <div class="row c-mt-30 c-mt-md-0">
            <div class="text-center img-wrap line col-md-12">
                <img src="assets/images/vertical_line2.png" alt="">
            </div>
            <div class="col-md-12">
                <div class="owl-carousel" data-autoplay="true" data-responsive-lg="1" data-responsive-md="1" data-responsive-sm="1" data-nav="false" data-dots="true" id="quote">
                    <div class="quote-item">
                        
                        <p class="small-text author-job">
                            {{trans('front.indexCustomertitle1')}}
                        </p>
                        <h5>
                            <a href="/">{{trans('front.indexCustomername1')}}</a>
                        </h5>
                        <p>
                            <em class="big">
                                {{trans('front.indexCustomerp1')}}                          
                            </em>
                        </p>

                    </div>
                    <div class="quote-item">
                        
                        <p class="small-text author-job">
                            {{trans('front.indexCustomertitle2')}}
                        </p>
                        <h5>
                            <a href="/">{{trans('front.indexCustomername2')}}</a>
                        </h5>
                        <p>
                            <em class="big">
                                {{trans('front.indexCustomerp2')}} 
                            </em>
                        </p>

                    </div>
                    <div class="quote-item">
                        
                        <p class="small-text author-job">
                            {{trans('front.indexCustomertitle3')}}
                        </p>
                        <h5>
                            <a href="/">{{trans('front.indexCustomername3')}}</a>
                        </h5>
                        <p>
                            <em class="big">
                                {{trans('front.indexCustomerp3')}} 
                            </em>
                        </p>

                    </div>
                    <div class="quote-item">
                        
                        <p class="small-text author-job">
                            {{trans('front.indexCustomertitle4')}}
                        </p>
                        <h5>
                            <a href="/">{{trans('front.indexCustomername4')}}</a>
                        </h5>
                        <p>
                            <em class="big">
                                {{trans('front.indexCustomerp4')}}                          
                            </em>
                        </p>

                    </div>
                    <div class="quote-item">
                        
                        <p class="small-text author-job">
                            {{trans('front.indexCustomertitle5')}}
                        </p>
                        <h5>
                            <a href="/">{{trans('front.indexCustomername5')}}</a>
                        </h5>
                        <p>
                            <em class="big">
                                {{trans('front.indexCustomerp5')}}                           
                            </em>
                        </p>

                    </div>
                    
                    <div class="quote-item">
    
                        <p class="small-text author-job">
                            {{trans('front.indexCustomertitle6')}}
                        </p>
                        <h5>
                            <a href="/">{{trans('front.indexCustomername6')}}</a>
                        </h5>
                        <p>
                            <em class="big">
                                {{trans('front.indexCustomerp6')}}
                            </em>
                        </p>

                    </div>
                    <div class="quote-item">
                        
                        <p class="small-text author-job">
                            {{trans('front.indexCustomertitle7')}} 
                        </p>
                        <h5>
                            <a href="/">{{trans('front.indexCustomername7')}}</a>
                        </h5>
                        <p>
                            <em class="big">
                                {{trans('front.indexCustomerp7')}}
                            </em>
                        </p>

                    </div>
                    <div class="quote-item">
                        
                        <p class="small-text author-job">
                            {{trans('front.indexCustomertitle8')}}
                        </p>
                        <h5>
                            <a href="/">{{trans('front.indexCustomername8')}}</a>
                        </h5>
                        <p>
                            <em class="big">
                                {{trans('front.indexCustomerp8')}}
                            </em>
                        </p>

                    </div>
                    <div class="quote-item">
                        
                        <p class="small-text author-job">
                            {{trans('front.indexCustomertitle9')}}
                        </p>
                        <h5>
                            <a href="/">{{trans('front.indexCustomername9')}}</a>
                        </h5>
                        <p>
                            <em class="big">
                                {{trans('front.indexCustomerp9')}}
                            </em>
                        </p>

                    </div>
                </div>
                <!-- .testimonials-slider -->
            </div>
            <div class="divider-55 d-none d-md-block"></div>
            <div class="text-center img-wrap col-md-12">
                <img src="assets/images/vertical_line2.png" alt="">
            </div>
            <div class="divider-10 d-none d-md-block"></div>
        </div>
    </div>
    <div class="testimonials-btn text-center">
        <a href="#quote" class="btn-maincolor">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
    <div class="corner corner-light"></div>
</section>

<section class="s-pt-130 s-pt-md-50 ls text-section">
    <div class="divider-30"></div>
    <div class="container">
        <div class="row">
            <div class="text-center col-md-12 justify-content-center text-block">
                <img src="assets/images/vertical_line.png" alt="">
                <div class="divider-35"></div>
                <div class="content">
                    <h1>
                        {{trans('front.indexStart1')}}
                        <br> {{trans('front.indexStart2')}}
                    </h1>
                    <p>
                        {{trans('front.indexStart3')}} 
                    </p>
                    <div class="divider-30"></div>
                </div>
                <img src="assets/images/vertical_line.png" alt="">
                <div>
                    <div class="divider-40"></div>
                    <a href="#contact" class="btn btn-outline-maincolor">{{trans('front.indexStartBtn')}}</a>
                    <div class="divider-30"></div>
                </div>
                <div class="img-wrap overflow-visible">
                    <img src="assets/images/vertical_line.png" alt="">
                    <div class="divider-5 d-none d-xl-block"></div>
                </div>
            </div>

        </div>
    </div>
</section>



@endsection
