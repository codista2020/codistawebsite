@extends('layouts.master')
@section('content')
<!-- About -->
<section id="aboutPage" class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
            <h1>{{trans('front.aboutPage')}}</h1>

                <div class="divider-15 d-none d-xl-block"></div>
            </div>
        </div>
    </div>
</section>
<section class="s-pt-30 s-pt-lg-50 ls about">
    <div class="divider-60 d-none d-xl-block"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="main-content text-center">
                    <h5>
                        {{trans('front.aboutPagep')}}
                    </h5>
                    <i class="rt-icon2-user"></i>

                    <div class="divider-10 d-none d-xl-block"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="s-pt-0  s-pb-30 s-pt-lg-30 s-pb-lg-75 ls about-icon teaser-contact-icon">
    <div class="divider-10 d-none d-xl-block"></div>
    <div class="container">
        <div class="row c-mt-50 c-mt-lg-0">
            <div class="col-lg-4 text-center call-icon">
                <div class="border-icon">
                    <div class="teaser-icon">
                        <img src="assets/images/icon1_about.png" alt="">
                    </div>
                </div>
                <h6>
                    {{trans('front.aboutPageWho')}}
                </h6>
                <div class="icon-content">
                    <p>
                        {{trans('front.aboutPageWhop')}}
                    </p>
                </div>
            </div>
            <div class="col-lg-4 text-center write-icon">
                <div class="divider-30 d-none d-xl-block"></div>
                <div class="border-icon">
                    <div class="teaser-icon">
                        <img src="assets/images/icon2_about.png" alt="">
                    </div>
                </div>
                <div class="icon-content">
                    <h6>
                        {{trans('front.aboutPageWhat')}}
                    </h6>
                    <p>
                        {{trans('front.aboutPageWhatp')}}
                    </p>
                </div>
            </div>
            <div class="col-lg-4 text-center visit-icon">
                <div class="border-icon">
                    <div class="teaser-icon">
                        <img src="assets/images/icon3_about.png" alt="">
                    </div>
                </div>
                <div class="icon-content">
                    <h6>
                        {{trans('front.aboutPageWhy')}}
                    </h6>
                    <p>
                        {{trans('front.aboutPageWhyp')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- #About Us -->

@endsection
