@extends('layouts.master')
@section('content')
<!-- Services -->
<section id="servicesPage" class="page_title ls s-py-50 corner-title ls invise overflow-visible">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
            <h1>{{trans('front.indexSliderBtn')}}</h1>
                <div class="divider-15 d-none d-xl-block"></div>
            </div>
        </div>
    </div>
</section>
<section id="servicesPageItem" class="ls s-pt-50 s-pb-100 s-pt-md-75 s-pt-lg-50 s-pb-lg-130 c-mb-30 service-item1">
    <div class="d-none d-lg-block divider-65"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="icon-box text-center hero-bg">
                    <div class="service-icon">
                        <img src="assets/images/service-icon-1.png" alt="">
                    </div>
                    <h6>
                        <span>{{trans('front.servicePageS1t')}}</span>
                    </h6>
                    <p>
                        {{trans('front.servicePageS1p')}}
                    </p>
                    <a class="book_service" href="/contact">
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        {{trans('front.servicePageBook')}}
                    </a>
                </div>
            </div>
            <!-- .col-* -->

            <div class="col-md-6 col-sm-12">
                <div class="icon-box text-center hero-bg">
                    <div class="service-icon">
                        <img src="assets/images/service-icon-2.png" alt="">
                    </div>
                    <h6>
                        <span>{{trans('front.servicePageS2t')}}</span>
                    </h6>
                    <p>
                        {{trans('front.servicePageS2p')}}
                    </p>
                    <a class="book_service" href="/contact">
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        {{trans('front.servicePageBook')}}
                    </a>
                </div>
            </div>
            <!-- .col-* -->

            <div class="col-md-6 col-sm-12">
                <div class="icon-box text-center hero-bg">
                    <div class="service-icon">
                        <img src="assets/images/service-icon-3.png" alt="">
                    </div>
                    <h6>
                        <span>{{trans('front.servicePageS3t')}}</span>
                    </h6>
                    <p>
                        {{trans('front.servicePageS3p')}}                    </p>
                    <a class="book_service" href="/contact">
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        {{trans('front.servicePageBook')}}
                    </a>
                </div>
            </div>
            <!-- .col-* -->

            <div class="col-md-6 col-sm-12">
                <div class="icon-box text-center hero-bg">
                    <div class="service-icon">
                        <img src="assets/images/service-icon-4.png" alt="">
                    </div>
                    <h6>
                        <span>{{trans('front.servicePageS4t')}}</span>
                    </h6>
                    <p>
                        {{trans('front.servicePageS4p')}}
                    </p>
                    <a class="book_service" href="/contact">
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        {{trans('front.servicePageBook')}}
                    </a>
                </div>
            </div>
            <!-- .col-* -->

            <div class="col-md-6 col-sm-12">
                <div class="icon-box text-center hero-bg">
                    <div class="service-icon">
                        <img src="assets/images/service-icon-5.png" alt="">
                    </div>
                    <h6>
                        <span>{{trans('front.servicePageS5t')}}</span>
                    </h6>
                    <p>
                        {{trans('front.servicePageS5p')}}
                    </p>
                    <a class="book_service" href="/contact">
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        {{trans('front.servicePageBook')}}
                    </a>
                </div>
            </div>
            <!-- .col-* -->
            <div class="col-md-6 col-sm-12">
                <div class="icon-box text-center hero-bg">
                    <div class="service-icon last">
                        <img src="assets/images/service-icon-6.png" alt="">
                    </div>
                    <h6>
                        <span>{{trans('front.servicePageS6t')}}</span>
                    </h6>
                    <p>
                        {{trans('front.servicePageS6p')}}
                    </p>
                    <a class="book_service" href="/contact">
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        {{trans('front.servicePageBook')}}
                    </a>
                </div>
            </div>
            <!-- .col-* -->
        </div>
    </div>
    <div class="d-none d-lg-block divider-45"></div>
</section>
<!-- #Services -->
@endsection
